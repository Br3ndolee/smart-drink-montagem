# __Montagem detalhada Smart Drink__

A montagem será dividida em 3 partes que se complementam, sendo elas:

1. Raspberry e Relê
2. Câmera, Iluminação, Sensor e Trava
3. Montagem Final

</br>


## __1. Raspberry e Relê__


Para montagem adequada do raspberry foi utilizado uma placa de acrilico de 15x10cm, como na imagem abaixo.



<img src="https://i.ibb.co/02DMFHx/IMG-20200615-122949.jpg" alt="Placa de Acrilico"  width="400">

 Colocar 8 espaçadores de plástico para fixar o Raspberry e o Relê.

<img src="https://i.ibb.co/G2VWrBz/IMG-20200615-123216.jpg" alt="Raspberry e relê na placa" width="400">


<p>Colocar 2 Espaçadores maiores para fixar a placa no fundo da geladeira.</p>
<img src=https://i.ibb.co/cc9Cpst/traseira.jpg alt="Raspberry e relê na placa" width="400">

Fixe a placa com 2 parafusos.

<img src=https://i.ibb.co/vHqvPJw/fix.jpg alt="Raspberry " width="400">

</br>

*   <p>Feito isso, agora é preciso conectar o terminais (GPIO) do Raspberry no relê.
      <p>Para isso deve-se utilizar 6 jumpers fxf de 10cm.</p>

       __Utilize o mapa de pinout abaixo__

    <img src=https://docs.microsoft.com/en-us/windows/iot-core/media/pinmappingsrpi/rp2_pinout.png alt="Raspberry Pinout" width="400">


      **<p>1. Conectar a alimentação do relê</p>**


    <img src=https://i.ibb.co/f0vDVxZ/rasp-rele-pwr.jpg alt="alimentação relê" width="400">

    PIN 2 (VCC) do Raspberry no PIN VCC do Relê e o PIN 6 (GND) do Raspberry no PIN GND do Relê

    </br>

    **<p>2. Conectar pinos de controle dos leds e da trava**

    -    **Conexão Raspberry/Relê Led Verde**
       
         <img src=https://i.ibb.co/Jd3zgvd/rasp-rele-ledv.jpg alt="led verde" width="400" >

         PIN 22 (GPIO 25) no pin IN1 do Relê

    -   **Conexão Raspberry/Relê Led Vermelho**
       
         <img src=https://i.ibb.co/3Br97DQ/rasp-rele-ledvm.jpg alt="led vermelho" width="400" >

         PIN 32 (GPIO 12) no pin IN2 do Relê

    -   **Conexão Raspberry/Relê Led Branco**
       
         <img src=https://i.ibb.co/nb6q12q/rasp-rele-ledb.jpg alt="led Branco" width="400" >

         PIN 16 (GPIO 23) no pin IN3 do Relê

    -   **Conexão Raspberry/Relê Trava Magnética**
       
         <img src=https://i.ibb.co/MsjygLt/rasp-rele-trava.jpg alt="led Branco" width="400" >

         PIN 18 (GPIO 24) no pin IN4 do Relê

</br>

## __2. Câmera, Iluminação, Sensor e Trava__

1.   __Câmera__

     Neste projeto foi utilizado uma Logitech HD 1080p.

     <img src=https://i.ibb.co/MSPSP5n/camera.jpg alt="camera" width="400">

     Como o suporte para camera impede a fixação adequada na geladeira, é preciso remover este suporte.
     <img src=https://i.ibb.co/VmPHwPw/camera-sem-sup.jpg alt="semsuporte" width="400">

     <p>Utilizando uma fita duplaface 3m cole a camera no centro do teto da geladeira.</p>
     <img src=https://i.ibb.co/KD4qhY7/cam-presa.jpg alt="semsuporte" width="400">

     <p>Plugue o cabo USB na entrada USB do raspberry</p>
     <img src=https://i.ibb.co/s1TxZrX/cam-rasp.jpg
      alt="cam_rasp" width="400">

</br>

2.  **Iluminação**
   
    Para iluminação adequada deve utilizar:
    -   2 Fita Led Branca de 20cm
    -   1 Fita Led Verde de 20cm
    -   1 Fita Led Vermelha de 20cm
    -   4 Resistor 100ohm
    -   Fios Preto e Vermelho
    -   Borne p4 fêmea

    Para cada Fita Led é preciso soldar um fio vermelho(de preferência) no terminal Positivo e um preto no Negativo. __*Para cada Fita Led precisa soldar um resistor em seu terminal negativo*__
    
    Assim:   
        <img src=https://i.ibb.co/SRTCVw1/IMG-20200615-135012.jpg alt="Solda Led" width="400" >

    Se possível isole o resistor com uma capa termoretrátil e após cubra a parte soldada com um termoretrátil ou com uma fita isolante.
        <img src=https://i.ibb.co/QpRcW6k/IMG-20200615-141047.jpg alt="Led" width="400">

    O tamanho do fio deve ser o suficiente para chegar até ao Relê.

    <p>Fixe as fitas brancas no teto da geladeira.</p>
    <img src=https://i.ibb.co/RQ7SJZw/fita-branca.jpg alt="branco" width="400">

    <p>Fixe a fita verde e a vermelha na frente proximo a porta.</p>
    <img src=https://i.ibb.co/GHkgJPG/fita-red-g.jpg alt="branco" width="400">

    Junte todos os fios __pretos__ (negativos) dos leds e plugue-os no __negativo__ do Borne. Feito isso, corte 4 fios __vermelhos__, de um tamaho equivalente para que se conecte do borne ao relê, que será posicionado no fundo.

    <img src=https://i.ibb.co/m9gpfqN/IMG-20200615-173345.jpg alt="borne" width="400">

    Pegue cada fio vermelho que sai do Borne e conecte terminal do meio de cada módulo do relê, faça isso nos 4 módulos.

    <img src=https://i.ibb.co/nR0F29R/modulo.png alt="modulo" width="400">

    Conecte o fio __positivo__ (vermelho) do led *verde* no módulo __k1__ do relê.

    <img src=https://i.ibb.co/zP3YzMN/IMG-20200615-174901.jpg alt="ledverde" width="400"> 

    Conecte o fio __positivo__ (vermelho) do led *vermelho* no módulo __k2__ do relê.

    <img src=https://i.ibb.co/5WZqgfp/IMG-20200615-175108.jpg alt="ledverm" width="400">
 
    Junte os fios __positivos__ (vermelho) das fitas led *brancas* e conecte ao módulo __k3__ do relê.

     <img src=https://i.ibb.co/HHR3TFH/IMG-20200615-175226.jpg alt="ledverm" width="400">
    

 </br>

 
3.  __Sensor Eletromagnético__
    
    O sensor virá com um fio pequeno e para isso será necessário emendar com outro fio para que fique o suficente a ligar no Raspberry.
    <img src=https://cdn.awsli.com.br/600x450/405/405912/produto/14145566/1b978cc234.jpg alt="sensor" width="400">

    Também será necessário soldar 2 jumpers fêmeas na ponta do fio, para que encaixe no terminal do Raspberry.

    <img src=https://i.ibb.co/GnjZPfr/IMG-20200615-172540.jpg alt="jumper no sensor" width="400">


     Coloque a parte do sensor *sem fio* na extremidade da porta proximo a tranca.

     <img src=https://i.ibb.co/T1DKFfW/semfio.jpg alt="semfio" width="400">

     A parte com fio deve ser fixada paralelamente com a parte sem fio para que o sensor consiga atuar.

     <img src=https://i.ibb.co/sgMnt0x/comfio.jpg alt="comfio" width="400">

    Feito isso, plugue os fios no terminal do Raspberry Pin 7 (GPIO 4) E Pin 9 (GND)

    <img src=https://i.ibb.co/FVJMWjY/rasp-sensor.jpg alt="sensor no rasp" width="400">

</br>

4.  __Trava Magnética__

     Para aclopar a trava na geladeira é necessáro fazer 2 furos na lateral da geladeira, é necessário que seja feito furos paralelos na porta também. As dimensões dependem do tamanho do parafuso e da trava.

     <img src=https://i.ibb.co/ws4BP9w/furos.png alt="furos" width="400">


     Com a trava em mãos, pegue 2 fios, um preto e um vermelho e emende nos fios da trava para que eles consigam chegar até o raspberry
     <img src=https://i.ibb.co/w05CRXc/trava.jpg
     alt="travamag" width="400">

     Coloque o suporte da trava.

     <img src=https://i.ibb.co/yRYWq8g/suporte.jpg alt="suporte" width="400">

     <p>Fixe-a na geladeira.</p>
     <img src=https://i.ibb.co/92hLXf5/presa.jpg alt="prender" width="400">

     <img src=https://i.ibb.co/kG7PJ9k/capa.jpg alt="capa" width="400">

     Passe o fio da trava entre a borracha da geladeira.

     <img src=https://i.ibb.co/d5NcMrK/passagem.jpg alt="borracha" width="400">

      Utilize alguma fita para fixar o fio entre a borracha e deixei uma folga o suficiente para abrir a porta sem arrebentar o fio.

     <img src=https://i.ibb.co/6ZSNpdW/folga.jpg
     alt="folga" width="400">


     Conecte o fio __positivo__ (vermelho) da *trava* no módulo __k4__ do relê. </br>
     
    <img src=https://i.ibb.co/s3nYMfR/IMG-20200615-175430.jpg alt="ledverm" width="400">

     E o __negativo__ (preto) junto com os outros fios netativos.

    <img src=https://i.ibb.co/m9gpfqN/IMG-20200615-173345.jpg alt="borne" width="400">

</br>

## __3. Montagem Final__

Após fixação dos componentes, caso tenha *temoretrátil* com grossura suficiente para passagem dos fios, passe todos os fios de alimentação dentro dele e fixe-o na lateral da geladeira. Se não for o caso, entrelace os fios vermelhos e separadamente os fios pretos.

<img src=https://i.ibb.co/WzRDTjR/dentro.jpg alt="dentro" width="400">

Com eles entrelaçados passe pela lateral da geladeira e fixe-os com fita isolante levando-os para o fundo afim de que passe para parte traseira da geladeira.

<img src=https://i.ibb.co/GCjgt9T/dentro-2.jpg alt="dentro2" width="400">

Cubra a tampa que cobria fan da geladeira e coloque duplaface em sua traseira para fixar ao fundo da geladeira.

<img src=https://i.ibb.co/8Pwb4gB/tampa.jpg alt="tampa" width="400">

No fundo, cole o borne p4 em alguma parte de fácil acesso.

<img src=https://i.ibb.co/vVCvp0w/borne.jpg alt="borne" width="400">

<p>Verifique se os jumpers e fios estão conectados corretamente. Siga este esquema para se orientar.</p>

<img src= https://i.ibb.co/sJW3xZG/circuito.png alt="circuito" width="400">

Se possível utilize um organizador de fios para segurar os fios da fonte do raspberry e do relê

<img src=https://i.ibb.co/NxChWWk/alimentacao.jpg alt="fios" width="400">
<p>*Se possível utilize organizadores de fio em toda a parte que cabos e fios ficarem soltos*</p>

Caso for utilizar uma tela para visualização, deverá plugar o cabo FLAT da tela no raspberry.

__ATENÇÃO!! O CABO É MUITO SENSÍVEL E PODE DANIFICAR A TELA__

<img src=https://i.ibb.co/GH9wz2p/cabo.jpg
alt="pronto" width="400">

__Após a montagem a geladeira devera ficar assim:__

<img src=https://i.ibb.co/x3z63qv/pronto.jpg
alt="pronto" width="400">

<img src=https://i.ibb.co/54p9mBV/tras.jpg
alt="pronto" width="400">

